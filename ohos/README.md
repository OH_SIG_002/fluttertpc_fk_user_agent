# fk_user_agent

Flutter插件，在Ohos设备上获取用户代理等属性

### Example user-agents:

| 系统   | User-Agent | WebView User-Agent |
|------| ---------- | ------------------ |
| Ohos | XXOS/5.0.0 (phone; OpenHarmony-5.0.0.60(Beta5)) | Mozilla/5.0 (phone; OpenHarmony-5.0.0.60(Beta5)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 ArkWeb/4.1.6.1 Mobile |

### Additionally:

接口常量和返回属性:
- systemName
- systemVersion
- packageName
- shortPackageName
- applicationName
- applicationVersion
- applicationBuildNumber
- packageUserAgent
- userAgent
- webViewUserAgent

### Usage:

```
  yaml
    dependencies:
      fk_user_agent: 2.1.0
      fk_user_agent_ohos: 1.0.0
 ```
